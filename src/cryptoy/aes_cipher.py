from cryptography.hazmat.primitives.ciphers.aead import (
    AESGCM,
)


def encrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    aesgcm = AESGCM(key)
    cipher = aesgcm.encrypt(nonce, msg, None)
    return cipher

def decrypt(cipher: bytes, key: bytes, nonce: bytes) -> bytes:
    aesgcm = AESGCM(key)
    plain = aesgcm.decrypt(nonce, cipher, None)
    return plain
