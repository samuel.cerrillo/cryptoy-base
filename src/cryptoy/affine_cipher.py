from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    perm = [(a * i + b) % n for i in range(n)]
    return perm


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    perm = compute_permutation(a, b, n)
    inv_perm = [perm.index(i) for i in range(n)]
    return inv_perm


def encrypt(msg: str, a: int, b: int) -> str:
    uni = str_to_unicodes(msg)
    en_uni = [(a * i + b) % 0x110000 for i in uni]
    en_msg = unicodes_to_str(en_uni)
    return en_msg


def encrypt_optimized(msg: str, a: int, b: int) -> str:
    uni = str_to_unicodes(msg)
    en_uni = [(a * i + b) % 0x110000 for i in uni]
    en_msg = unicodes_to_str(en_uni)
    return en_msg


def decrypt(msg: str, a: int, b: int) -> str:
    uni = str_to_unicodes(msg)
    aff_keys = compute_affine_keys(0x110000)
    a_inv = compute_affine_key_inverse(a, aff_keys, 0x110000)
    de_uni = [(a_inv * (i - b)) % 0x110000 for i in uni]
    de_msg = unicodes_to_str(de_uni)
    return de_msg


def decrypt_optimized(msg: str, a_inv: int, b: int) -> str:
    uni = str_to_unicodes(msg)
    de_uni = [(a_inv * (i - b)) % 0x110000 for i in uni]
    de_msg = uni_to_str(de_uni)
    return de_msg


def compute_affine_keys(n: int) -> list[int]:
    aff_keys = [a for a in range(1, n) if gcd(a, n) == 1]
    return aff_keys


def compute_affine_key_inverse(a: int, aff_keys: list, n: int) -> int:
    for a_inv in aff_keys:
        if (a * a_inv) % n == 1:
            return a_inv
    raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"

    b = 58
    aff_keys = compute_affine_keys(0x110000)

    for a in range(1, 0x110000):
        try:
            a_inv = compute_affine_key_inverse(a, aff_keys, 0x110000)
        except:
            continue
        de_msg = de_opti(s, a_inv, b)
        if "bombe" in de_msg:
            return de_msg, (a, b)

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )

    aff_keys = compute_affine_keys(0x110000)

    for a in range(0x110000):
        try:
            a_inv = compute_affine_key_inverse(a, aff_keys, 0x110000)
        except:
            continue
        for b in range(1, 10001):
            de_msg = de_opti(s, a_inv, b)
            if "bombe" in de_msg:
                return de_msg, (a, b)

    raise RuntimeError("Failed to attack")

