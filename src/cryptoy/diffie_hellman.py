import random
import sys

from cryptoy.utils import (
    pow_mod,
)

sys.setrecursionlimit(5000)


def keygen(prime_nb: int, generator: int) -> dict[str, int]:
    pri_k = random.randint(2, prime_nb - 1)
    pub_k = pow_mod(generator, pri_k, prime_nb)
    return {"public_key": pub_k, "private_key": pri_k}


def compute_shared_secret_key(public: int, private: int, prime_nb: int) -> int:
    sh_sec_k = pow_mod(public, private, prime_nb)
    return sh_sec_k
